package com.freshflow.davdroid.syncadapter;


import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListAdapter;

import at.bitfire.davdroid.R;
import at.bitfire.davdroid.syncadapter.AccountDetailsFragment;
import at.bitfire.davdroid.syncadapter.ServerInfo;

/**
 * Created by Jan
 */
public class SelectCollectionsFragment extends at.bitfire.davdroid.syncadapter.SelectCollectionsFragment {

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.next:
                    ServerInfo serverInfo = (ServerInfo)getArguments().getSerializable(KEY_SERVER_INFO);

                    // synchronize only selected collections
                    for (ServerInfo.ResourceInfo addressBook : serverInfo.getAddressBooks())
                        addressBook.setEnabled(false);
                    for (ServerInfo.ResourceInfo calendar : serverInfo.getCalendars())
                        calendar.setEnabled(false);

                    ListAdapter adapter = getListView().getAdapter();
                    for (long id : getListView().getCheckedItemIds()) {
                        int position = (int)id + 1;		// +1 because header view is inserted at pos. 0
                        ServerInfo.ResourceInfo info = (ServerInfo.ResourceInfo)adapter.getItem(position);
                        info.setEnabled(true);
                    }

                    // pass to "account details" fragment
                    AccountDetailsFragment accountDetails = new AccountDetailsFragment();
                    Bundle arguments = new Bundle();
                    arguments.putSerializable(at.bitfire.davdroid.syncadapter.SelectCollectionsFragment.KEY_SERVER_INFO, serverInfo);
                    accountDetails.setArguments(arguments);

                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_login_container, accountDetails)
                            .addToBackStack(null)
                            .commitAllowingStateLoss();
                    break;
                default:
                    return false;
            }
            return true;
        }
 }
