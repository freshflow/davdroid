package com.freshflow.davdroid.syncadapter;

import android.content.Loader;
import android.os.Bundle;
import android.widget.Toast;

import at.bitfire.davdroid.R;
import com.freshflow.davdroid.syncadapter.SelectCollectionsFragment;
import at.bitfire.davdroid.syncadapter.ServerInfo;

/**
 * Created by Jan
 */
public class QueryServerDialogFragment extends at.bitfire.davdroid.syncadapter.QueryServerDialogFragment {
    @Override
    public void onLoadFinished(Loader<ServerInfo> loader, ServerInfo serverInfo) {
        if (serverInfo.getErrorMessage() != null)
            Toast.makeText(getActivity(), serverInfo.getErrorMessage(), Toast.LENGTH_LONG).show();
        else {
            SelectCollectionsFragment selectCollections = new SelectCollectionsFragment();
            Bundle arguments = new Bundle();
            arguments.putSerializable(SelectCollectionsFragment.KEY_SERVER_INFO, serverInfo);
            selectCollections.setArguments(arguments);

            getFragmentManager().beginTransaction()
                    .replace(R.id.fragment_login_container, selectCollections)
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        }

        getDialog().dismiss();
    }
}
