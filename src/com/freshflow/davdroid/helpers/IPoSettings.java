package com.freshflow.davdroid.helpers;

/**
 * Created by Jan
 */
public interface IPoSettings {
    String getProtocol();
    String getServer();
    Boolean isPreemptiveAuth();
    String getUrl();
}
