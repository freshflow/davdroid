package com.freshflow.davdroid.helpers;

import android.content.Context;

import java.util.Properties;

/**
 * Created by Jan
 */
public class PoSettings implements IPoSettings {
    public static String PROTOCOL_HTTP = "http://";
    public static String PROTOCOL_HTTPS = "https://";
    public static String PROPERTIES_FILE = "po.properties";
    public static String SECURE_PROTOCOL = "secure_protocol";
    public static String SERVER_LIVE = "server_live";
    public static String SERVER_TEST = "server_test";
    public static String PREEMPTIVE_AUTH = "preemptive_auth";
    public static String ENVIRONMENT = "environment";

    private Context context;
    private AssetsPropertyReader reader;
    private Properties properties;

    public PoSettings(Context context) {
        this.context = context;
        reader = new AssetsPropertyReader(context);
        properties = reader.getProperties(PROPERTIES_FILE);

    }

    public String getProtocol() {
        if(properties.getProperty(SECURE_PROTOCOL).equals("true")) {
            return PROTOCOL_HTTPS;
        }
        return PROTOCOL_HTTP;
    }

    @Override
    public String getServer() {
        if(properties.getProperty(ENVIRONMENT).equals("production")) {
            return properties.getProperty(SERVER_LIVE);
        }
        return properties.getProperty(SERVER_TEST);
    }

    @Override
    public Boolean isPreemptiveAuth() {
        if(properties.getProperty(PREEMPTIVE_AUTH).equals("true")) {
            return true;
        }
        return false;
    }

    @Override
    public String getUrl() {
        return getProtocol() + getServer();
    }
}
