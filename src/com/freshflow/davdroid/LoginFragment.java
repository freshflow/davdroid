package com.freshflow.davdroid;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.freshflow.davdroid.helpers.PoSettings;
import com.freshflow.davdroid.syncadapter.QueryServerDialogFragment;

import at.bitfire.davdroid.R;
import at.bitfire.davdroid.URIUtils;

/**
 * Created by root on 13.7.14.
 */
public class LoginFragment extends Fragment {
    private EditText username;
    private EditText password;
    private Button login;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.login_fragment, container, false);

        // input fields
        username = (EditText) v.findViewById(R.id.po_username);
        password = (EditText) v.findViewById(R.id.po_password);
        login = (Button) v.findViewById(R.id.po_login);

        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                login();
            }
        });

        return v;
    }

    public void login() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        PoSettings settings = new PoSettings(this.getActivity());

        Bundle args = new Bundle();
        args.putString(QueryServerDialogFragment.EXTRA_BASE_URL, URIUtils.sanitize(settings.getUrl()));
        args.putString(QueryServerDialogFragment.EXTRA_USER_NAME, username.getText().toString());
        args.putString(QueryServerDialogFragment.EXTRA_PASSWORD, password.getText().toString());
        args.putBoolean(QueryServerDialogFragment.EXTRA_AUTH_PREEMPTIVE, settings.isPreemptiveAuth());

        DialogFragment dialog = new QueryServerDialogFragment();
        dialog.setArguments(args);
        dialog.show(ft, QueryServerDialogFragment.class.getName());
    }
}