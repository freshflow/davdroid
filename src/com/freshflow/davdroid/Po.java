package com.freshflow.davdroid;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.freshflow.davdroid.helpers.PoSettings;

import at.bitfire.davdroid.R;
import at.bitfire.davdroid.URIUtils;
import com.freshflow.davdroid.syncadapter.QueryServerDialogFragment;

/**
 * Created by Jan
 */
public class Po extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        if (savedInstanceState == null) {	// first call
            getFragmentManager().beginTransaction()
                    .add(R.id.fragment_login_container, new LoginFragment(), "login_fragment")
                    .commit();
        }
    }
}